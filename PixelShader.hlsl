#define MAX_STEPS 100
#define MAX_DIST 100.0f
#define SURF_DIST 0.001f

//Define a mod as hlsl's works slightly differently
#define goodmod(x, y) ((x) - (y) * floor((x) / (y)))

cbuffer ScreenSizeBuffer : register(b1){
	float2 screenSize;
}

cbuffer TimeBuffer : register(b2){
	float time;
}

float4 main(float2 tex : TexCoord) : SV_Target{
	float2 pixelPos = float2(tex.x * screenSize.x, tex.y * screenSize.y);
	float2 uv = (pixelPos - 0.5f * screenSize) / screenSize.y;

	const float zoomFactor = cos(time * 0.3f) * 0.5f + 0.5f;

	//Camera
	float3 rayOrigin = float3(sin(time * 0.2f), 0, cos(time * 0.2f));
	float3 lookAt = float3(sin(time) * 0.2f, cos(time * 0.5f) * 0.1f, 0);
	float zoom = 1 / (1 + pow(2.71828, zoomFactor - 0.5f)); //Logistic curve
	
	float3 forward = normalize(lookAt - rayOrigin);
	float3 right = cross(float3(0, 1, 0), forward);
	float3 up = cross(forward, right);
	
	float3 center = rayOrigin + (forward * zoom); //Center of our virtual screen
	float3 intersection = center + (uv.x * right) + (uv.y * up); //Where our ray intersects the virtual screen
	float3 rayDirection = normalize(intersection - rayOrigin);
	
	//Ray march
	float distToSurface = 0.0f;
	float dist = 0.0f;
	
	float3 pos;
	
	for(int i = 0; i < MAX_STEPS; ++i){
		pos = rayOrigin + (rayDirection * dist);
		
		distToSurface = -(length(float2(length(pos.xz) - 1, pos.y)) - 0.75f); //SDF for a torus
		if(distToSurface < SURF_DIST){ //If we're close enough to the surface then consider it a hit
			break;
		}
		
		dist += distToSurface; //Step forward the estimated distance to the surface
	}
	
	float3 col = float3(0, 0, 0);
	
	if(distToSurface < SURF_DIST){
		//Get polar coords
		float x = atan2(pos.x, pos.z); //Offsets the stripes
		float y = atan2(length(pos.xz) - 1, pos.y); //Basically how many stripes (used part of the torus in this)
		
		float bands = sin((y * 5) + (x * 20)); //The bands twisting in the torus
		float pulse = sin((x * 2) + (y * 8) + time * 2.0f); //Pulse moving through vertically
		float ripples = sin((((x * 5) + (y * 20)) * 0.5) - time * 1.5f) * 0.5 + 0.5;
		
		float step1 = smoothstep(-0.1, 0.1, bands); //Smooths the edges of the bands
		float step2 = smoothstep(-0.1, 0.1, bands - 0.5f); //As above but smaller
		float boarders = step1 * 1 - step2; //Creates boarders
		
		float mask = boarders * ripples;
		
		float3 checkerBoard = max(mask, bands * pulse);

		col += checkerBoard;
	}

	return float4(col, 1.0f);
}